const jwt = require('jsonwebtoken')

const secret = "CSP2_E-Commerce"


module.exports.createAccessToken = user => {
    // data from the registration form
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    return jwt.sign(data, secret,{})
}

module.exports.verify = (req,res, next) =>{
    let token = req.headers.authorization
    if(typeof token !== 'undefined'){
        token=token.slice(7, token.length)
        return jwt.verify(token, secret, (err, data)=>{
            if(err)
                return res.send({auth: "failed"})
            else
                next()
        })
    // Token does not exist
    }else{
        return res.send({auth: "No token found"})
    }
}

module.exports.decode = token=>{
    if (typeof token !== 'undefined'){
        token=token.slice(7, token.length)
        return jwt.verify(token, secret, (err, data) =>{
            if(err)
                return null
            else
                return jwt.decode(token, {complete:true}).payload
        })
    // Token does not exist
    }else{
        return null
    }
}