const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required : [true, "First name is required"]
    },
    lastName:{
        type: String,
        required : [true, "Last name is required"]
    },
    email:{
        type: String,
        required : [true, "Email is required"]
    },
    password:{
        type: String,
        required : [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orderedProduct: [
        {
            products:[
                {
                    productId: {
                        type: mongoose.Schema.ObjectId,
                        required: [true, "Product ID is required"]
                    },
                    productName: {
                        type: String,
                        required: [true, "Product Name is required"]
                    },
                    quantity: {
                        type: Number,
                        default: 1
                    }
                }
            ],
            totalAmount: {
                type: Number,
                default: 0
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
        }
    ],
    pic: String
})

module.exports = mongoose.model("User", userSchema)
