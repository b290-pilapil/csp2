const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name:{
        type: String,
        required : [true, "Product Name is required"]
    },
    description:{
        type: String,
        required : [true, "Description is required"]
    },
    price:{
        type: Number,
        required : [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    userOrders: [
        {
            userId: {
                type: mongoose.Schema.ObjectId,
                required: [ true, "User ID is required"]
            }, 
            orderId: mongoose.Schema.ObjectId
        }
    ],
    pic: String
})

module.exports = mongoose.model("Product", productSchema)
