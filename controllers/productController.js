const Product = require('../models/Product')

module.exports.addProduct = reqBody => {
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    })
    return newProduct.save()
        .then(product=> {
            return {"success": true,"message":`${product.name} has been successfullly added to the database.`}
        })
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in adding product. Please try again later."}
        })
}

module.exports.getAllProducts = ()=>{
    return Product.find({})
        .then(result => (result.length==0)?{"fail_message":"There are no products to display"}:result)
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in retrieving products. Please try again later."}
        })
}

module.exports.getAllActiveProducts = ()=>{
    return Product.find({isActive: true})
        .then(result => (result.length==0)?{"fail_message":"There are no products to display"}:result)
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in retrieving products. Please try again later."}
        })
}

module.exports.getProduct = (id)=>{
    return Product.findById(id)
        .then(result => (result!=null)?result:{"fail_message":'Product not found.'})
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in retrieving product. Please try again later."}
        })
}

module.exports.getHotProducts = ()=>{
    return Product.aggregate([
        {
            $match:
                { userOrders: { $ne: [], }, },
        },
        {
            $project: { name: 1, description: 1, price: 1, pic: 1,
              count: { $size: "$userOrders", },
            },
        },
        { $limit: 3, },
      ])
        .then(result => (result!=null)?result:{"fail_message":'No product not found.'})
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in retrieving products. Please try again later."}
        })
}

module.exports.updateProduct = (id, reqBody)=>{
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        isActive: reqBody.isActive
    }
    // https://stackoverflow.com/questions/286141/remove-blank-attributes-from-an-object-in-javascript
    // remove null fields, prevent them from being written into the DB
    Object.keys(updatedProduct).forEach((k) => updatedProduct[k] == null && delete updatedProduct[k]);
    
    // https://www.freecodecamp.org/news/check-if-an-object-is-empty-in-javascript/
    if(JSON.stringify(updatedProduct) !== "{}"){
        return Product.findByIdAndUpdate(id, updatedProduct)
        .then(result => {
            return {"success": true,"message":`Successfully updated ${(updatedProduct.name!=='' && updatedProduct.name!==undefined?updatedProduct.name:'product')}`}
        })
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in updating product. Please try again later."}
        })
    }else{
        return {"fail_message":"[Message Requires Revision]\nNo changes done. Please check property names are correct."}
    }
}

module.exports.archiveProduct = (id)=>{
    return Product.findByIdAndUpdate(id, {isActive: false})
        .then(result => {
            return {"success": true,"message":"Successfully archived product."}
        })
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in updating product. Please try again later."}
        })
}

module.exports.activateProduct = (id)=>{
    return Product.findByIdAndUpdate(id, {isActive: true})
        .then(result => {
            return {"success": true,"message":"Successfully activated product"}
        })
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in updating product. Please try again later."}
        })
}