// Used during register & login
const bcrypt = require('bcrypt')

const User = require('../models/User')
const Product = require('../models/Product')

// Used only during login
const auth = require('../auth')

module.exports.registerUser = reqBody => {
    return User.find({email: reqBody.email})
        .then(result=>{
            if(result.length>0){
                return {"fail_message":"The entered email is already in use."}
            }else{
                let newUser = new User({
                    firstName: reqBody.firstName,
                    lastName: reqBody.lastName,
                    email: reqBody.email,
                    password: bcrypt.hashSync(reqBody.password,10)
                })
                return newUser.save()
                    .then(user=> {
                        return {"success": true,"message":`Successfully registered! Welcome, ${newUser.firstName}!`}
                    })
                    .catch(error => {
                        console.log(error)
                        return {"fail_message":"There are missing/empty values. Please input all required fields and try again."}
                    })
            }
        }).catch(error => {
            console.log(error)
            return {"fail_message":"Please input all required fields try again."}
        })
}

module.exports.loginUser = (reqBody)=>{
    return User.findOne({email: reqBody.email})
        .then(result=>{
            if(result==null){
                return {"fail_message":'This email is not registered.'}
            }else{
                const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
                if(isPasswordCorrect){
                    const welcomeBackMessages = [
                        `Welcome back, ${result.firstName}! We missed you!`,
                        `${result.firstName}, so glad you're back with us!`,
                        `Welcome back, ${result.firstName}! Let's catch up!`,
                        `${result.firstName}, we're happy to have you back!`,
                        `Welcome back, ${result.firstName}! Ready to rock and roll?`,
                        `${result.firstName}, you were missed. Welcome back!`,
                        `Welcome back, ${result.firstName}! Let's continue where we left off.`,
                        `${result.firstName}, back in action! Welcome back!`,
                        `Welcome back, ${result.firstName}! We're better with you here.`,
                        `${result.firstName}, you were gone too long. Welcome back!`
                      ];
                    return {
                        message: welcomeBackMessages[Math.floor((Math.random()*10))],
                        access: auth.createAccessToken(result)
                    }
                }else{
                    return {"fail_message":'Email or Password is incorrect.'}
                }
            }
        }).catch(error => {
            console.log(error)
            return {"fail_message":"Error in logging in. Please try again later."}
        })
}

module.exports.getDetails = (id)=>{
    return User.findById(id)
        .then(result => {
            result.password=''
            return result
        })
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in retrieving user. Please try again later."}
        })
}

module.exports.getAllDetails = ()=>{
    return User.find({}, {email: 1, isAdmin: 1})
        .then(result => {
            return result
        })
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in retrieving user. Please try again later."}
        })
}

// aka checkout
module.exports.createOrder = (id, reqBody)=>{
    let orderedProduct ={
        products: [],
        totalAmount: 0
    }
    reqBody.products.forEach(product => {
        // verify if product exists 
        // Product.findById(product.productId)
        //     .then(result => {
                // userOrders.push(product.productId)
                orderedProduct.products.push(product)
                orderedProduct.totalAmount+=(product.quantity*product.price)
            //     return true
            // }).catch(error => {
            //     console.log(error)
            //     return false
            // })
    })
    return User.findById(id)
    .then(result=> {
            // console.log(orderedProduct)
            result.orderedProduct.push(orderedProduct)
            return result.save()
                .then(result => {
                    result.orderedProduct[result.orderedProduct.length-1].products.forEach(element => {
                        Product.findById(element.productId)
                            .then(prodResult=> {
                                prodResult.userOrders.push({userId: id})
                                prodResult.save()
                                    .then(prodResult => true)
                                    .catch(error => {
                                        console.log(error)
                                        return {"fail_message": "Error in checking out your order. Please try again later."}
                                    })
                            })
                            .catch(prodError => {
                                console.log(prodError)
                                return {"fail_message": "Error in checking out your order. Please try again later."}
                            })
                    })
                    return {"success": true}
                })
                .catch(userError => {
                    console.log(userError)
                    return {"fail_message": "Error in checking out your order. Please try again later."}
                })
        }).catch(error => {
            console.log(error)
            return {"fail_message":"Error in retrieving product/s. Please try again later."}
        })
}

module.exports.setAsAdmin = (email)=>{
    return User.findOneAndUpdate({email: email}, {isAdmin: true})
        .then(result => {
            return {"success":true, "message":"User has been successfully elevated to the Admin role"}
        })
        .catch(error => {
            console.log(error)
            return {"fail_message":"Error in updating user. Please try again later."}
        })
}

module.exports.getOrders = (id)=>{
    return User.findById(id, 'orderedProduct')
    .then(result => {
        return result
    })
    .catch(error => {
        console.log(error)
        return {"fail_message":"Error in retrieving orders. Please try again later."}
    })
}

module.exports.getAllOrders = ()=>{
    return User.find({'orderedProduct': {$ne: []}}, {'orderedProduct': 1, 'email': 1})
    .then(result => {
        return result
    })
    .catch(error => {
        console.log(error)
        return {"fail_message":"Error in retrieving orders. Please try again later."}
    })
}

module.exports.changePassword = (id,reqBody)=>{
    return User.findById(id)
        .then(result=>{
            if(result==null){
                return {"fail_message":'This user was not found.'}
            }else{
                const isPasswordCorrect = bcrypt.compareSync(reqBody.oldPass, result.password)
                if(isPasswordCorrect){
                    result.password= bcrypt.hashSync(reqBody.newPass,10)
                    return result.save()
                        .then(user=> {
                            return {"success": true,"message":`Successfully changed password!`}
                        })
                        .catch(error =>{
                            console.log(error)
                            return {"fail_message":"Error in saving changes. Please try again later."}
                        })
                }else{
                    return {"fail_message":'Inputted password is incorrect.'}
                }
            }
        }).catch(error2 => {
            console.log(error2)
            return {"fail_message":"Error in changing password. Please try again later."}
        })
}
