const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const userRoute = require('./routes/userRoute')
const productRoute = require('./routes/productRoute')

const app = express()
// "mongodb://admin:admin123@10.10.10.16:27017/e-commerce",
mongoose.connect(
    'mongodb+srv://admin:admin123@zuitt.p89vf9y.mongodb.net/e-commerce?retryWrites=true&w=majority', 
    {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once('open', ()=>console.log('Now connected to MongoDB Atlas.'))

// middleware
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// routes
app.get('/', (req,res)=>{
    res.send('We gucci now')
})

app.use('/users', userRoute)
app.use('/products', productRoute)

if(require.main === module){
    app.listen(process.env.PORT || 4000, ()=> {console.log(`API online on port ${process.env.PORT || 4000}`)})
}

module.exports = {app, mongoose}