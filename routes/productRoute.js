const express = require('express')
const router = express.Router()

const auth = require('../auth')
const productController = require('../controllers/productController')

router.post('/add', auth.verify, (req, res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        if(req.body.name!==undefined && req.body.name!=='' && req.body.description!==undefined && req.body.description!=='' && req.body.price!==undefined && req.body.price!=='' && req.body.price!==0){
            productController.addProduct(req.body)
                .then(controllerResult => res.send(controllerResult))
        }else{
            res.send("Error. There are missing/empty values. Please input all required fields and try again.")
        }
    }else{
        res.send('Cannot Proceed with this action. User is not an admin.')
    }
})

router.get("/all", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        productController.getAllProducts()
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send('Cannot Proceed with this action. User is not an admin.')
    }
})

router.get("/", (req,res)=>{
    productController.getAllActiveProducts()
        .then(controllerResult => res.send(controllerResult))
})

router.get("/hot", (req,res)=>{
    productController.getHotProducts()
        .then(controllerResult => res.send(controllerResult))
})

router.get("/:productId", (req,res)=>{
    productController.getProduct(req.params.productId)
        .then(controllerResult => res.send(controllerResult))
})

router.put("/:productId", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        productController.updateProduct(req.params.productId, req.body)
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send('Cannot Proceed with this action. User is not an admin.')
    }
})

router.patch("/:productId/archive", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        productController.archiveProduct(req.params.productId)
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send('Cannot Proceed with this action. User is not an admin.')
    }
})

router.patch("/:productId/activate", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        productController.activateProduct(req.params.productId)
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send('Cannot Proceed with this action. User is not an admin.')
    }
})


module.exports = router