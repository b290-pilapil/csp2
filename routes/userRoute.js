const express = require('express')
const router = express.Router()

const auth = require('../auth')
const userController = require('../controllers/userController')

router.post('/register', (req, res)=>{
    userController.registerUser(req.body)
        .then(controllerResult => res.send(controllerResult))
})

router.post('/login', (req,res)=>{
    // error checking here since if/else breaks .then() in the controller
    if(req.body.email!==undefined && req.body.email!=='' && req.body.password!==undefined && req.body.password!==''){
        userController.loginUser(req.body)
            .then(controllerResult=>res.send(controllerResult))
    }else{
        res.send("Error. There are missing/empty values. Please input email and password and try again.")
    }
})

router.put("/checkout", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===false){
        userController.createOrder((auth.decode(req.headers.authorization).id), req.body)
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send('Cannot Proceed with this action. Admins cannot checkout.')
    }
})

router.get("/userDetails", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    userController.getDetails(userData.id)
        .then(controllerResult => res.send(controllerResult))
})

router.patch("/changePassword", auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    userController.changePassword(userData.id, req.body)
        .then(controllerResult => res.send(controllerResult))
})

router.get("/all", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        userController.getAllDetails()
           .then(controllerResult => res.send(controllerResult))
    }
})

router.patch("/setAsAdmin", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        if(req.body.email!==undefined && req.body.email!==''){
            userController.setAsAdmin(req.body.email)
                .then(controllerResult => res.send(controllerResult))
        }else{
            res.send("Cannot Proceed with this action. No email has been provided")
        }
    }else{
        res.send('Cannot Proceed with this action. User is not an admin.')
    }
})

router.get("/myOrders", auth.verify, (req,res)=>{
    userController.getOrders(auth.decode(req.headers.authorization).id)
        .then(controllerResult => res.send(controllerResult))
})

router.get("/orders", auth.verify, (req,res)=>{
    if(auth.decode(req.headers.authorization).isAdmin===true){
        userController.getAllOrders()
            .then(controllerResult => res.send(controllerResult))
    }else{
        res.send('Cannot Proceed with this action. User is not an admin.')
    }
})

module.exports = router